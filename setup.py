#!/usr/bin/python
# -*- coding: utf-8
from setuptools import setup, find_packages

setup(
    name='subimage',
    author='Selçuk Karakayalı',
    author_email='skarakayali@gmail.com',
    maintainer='Selçuk Karakayalı',
    url='http://bitbucket.com/karakays/waldo/',
    packages=find_packages(),
    install_requires=['opencv-python>=3.4.2',
                      'pillow>=5.2.0'],
    python_requires='>=3',
    description='Challenge of Waldo Photos Engineering',
    scripts=['bin/subimage']
)
