import argparse
import cv2
import logging


logger = logging.getLogger(__name__)


def parse_args():
    import numpy as np
    from PIL import Image

    def valid_image(path):
        try:
            im = Image.open(path)
            np_arr = np.asarray(im)
            return np_arr
        except IOError:
            raise argparse.ArgumentTypeError('Bad image: %s' % path)
        except Exception:
            logger.error('Something\'s wrong here', exc_info=1)
            exit(1)

    parser = argparse.ArgumentParser()

    parser.add_argument('-v', '--verbose', help='verbose output',
                        action='store_const', dest='loglevel',
                        const=logging.DEBUG, default=logging.INFO)

    parser.add_argument('image1', type=valid_image, help='image file path')
    parser.add_argument('image2', type=valid_image, help='image file path')

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)

    return args


def subimage(input_im, temp_im):
    """
    Comparing both images pixel by pixel. Only works for exact matches.
    :param input_im: Numpy array of input image
    :param temp_im: Numpy array of template image
    :returns Tuple of pixel location of match or none, otherwise
    """
    for r in range(0, input_im.shape[0] - temp_im.shape[0] + 1):
        for c in range(0, input_im.shape[1] - temp_im.shape[1] + 1):
            crp_row, crp_col = (r + temp_im.shape[0],
                                c + temp_im.shape[1])

            crop = input_im[r:crp_row, c:crp_col]

            matches = (crop == temp_im)

            if matches.all():
                return (r, c)


def subimage_tm(input_im, temp_im, corr_th=0.95):
    """
    Template matching using OpenCV CCOEFF_NORMED method.
    :param input_im: Numpy array of input image
    :param temp_im: Numpy array of template image
    :param corr_th: Correlation threshold to match template
    :returns Tuple of pixel location of match or none, otherwise
    """
    result = cv2.matchTemplate(input_im, temp_im, cv2.TM_CCOEFF_NORMED)
    minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(result)
    logger.debug('Max correlation=%s', maxVal)

    return maxLoc if maxVal >= corr_th else None


def main():
    args = parse_args()

    im1, im2 = ((args.image1, args.image2)
                if args.image1.size > args.image2.size
                else (args.image2, args.image1))

    logger.debug('im1.shape=%s, im2.shape=%s', im1.shape, im2.shape)

    pixels = subimage_tm(im1, im2)

    if pixels:
        logger.info('Matches at %s', pixels)
    else:
        logger.info('No match')


if __name__ == '__main__':
    main()
