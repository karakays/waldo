=======
subimage
=======
Specification can be found `here`_

------------
Requirements
------------
* Python version 3

------------
Installation
------------
You may need root access during installation.

.. code:: bash

    $ git clone https://bitbucket.org/karakays/waldo.git
    $ cd waldo && python3 setup.py install
    $ subimage --help

.. _`here`: https://gist.github.com/pkoz/0b5f8b75a07785430a2e9d2698316b13
